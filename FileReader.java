import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;

import animals.*;

public final class FileReader {

	private static Map<String, Function<String, Animal>> activators = new HashMap<>() {
		{
			put("Dog", x -> new Dog(x));
			put("SmallDog", x -> new SmallDog(x));
			put("Spider", x -> new Spider(x));
			put("Giraffe", x -> new Giraffe(x));
			put("Mule", x -> new Mule(x));
		}
	};

	public static Animal[] parseAnimalsFromFile(String fileName) throws FileNotFoundException, InvalidAnimalException {

		Scanner fileReader = new Scanner(new File(fileName));
		List<Animal> animals = new ArrayList<>();
		while (fileReader.hasNextLine()) {
			Scanner lineReader = new Scanner(fileReader.nextLine());
			String type = lineReader.next();
			String name = lineReader.next();
			lineReader.close();
			if (activators.containsKey(type)) {
				Animal animal = activators.get(type).apply(name);
				animals.add(animal);
			} else {
				throw new InvalidAnimalException();
			}
		}
		fileReader.close();
		return animals.toArray(Animal[]::new);
	}
}
