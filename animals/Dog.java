package animals;

public class Dog extends Animal implements IPettable, IFeedable {

	public Dog() {
		this("unnamed");
	}

	public Dog(String name) {
		super(name);
		this.legs = 4;
	}

	@Override
	public void speak() {
		System.out.println("Dog: Woof!");
	}

	public void shit() {
		System.out.println("Everyone: Wääääää");
	}

	@Override
	public void feed() {
		System.out.println("Dog: Nom nom nom");
	}

	@Override
	public void pet() {
		System.out.println("You: Who's a good boy?");
	}

	@Override
	public void scold() {
		System.out.println("You: Bad dog!");
	}
}