package animals;

public class Giraffe extends Animal implements IPettable, IFeedable {

	public Giraffe(String name) {
		super(name);
		this.legs = 4;
	}

	@Override
	public void feed() {
		System.out.println("You: Wait what the fuck do giraffes eat");
	}

	@Override
	public void pet() {
		System.out.println("You: Sorry but I can't reach you...");
	}

	@Override
	public void scold() {
		System.out.println("Fuck off back to Africa!!!");
	}

	@Override
	public void speak() {
		System.out.println("What does the Giraffe say!?");
	}
}
