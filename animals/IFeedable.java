package animals;

public interface IFeedable {
	public void feed();
}