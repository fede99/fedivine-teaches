package animals;

public class SmallDog extends Dog {

	public SmallDog() {
		super();
	}

	public SmallDog(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return super.toString() + " MiniWoof!";
	}

	@Override
	public void speak() {
		System.out.println("Mini Woof!");
	}
}