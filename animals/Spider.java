package animals;

public class Spider extends Animal {

	public Spider() {
		this("unnamed");
	}

	public Spider(String name) {
		super(name);
		this.legs = 8;
	}

	@Override
	public void speak() {
		System.out.println("Hmmmmm");
	}
}