package animals;

public interface IPackingAnimal<T> {

	public void store(String key, T item);

	public T retreive(String key);
}