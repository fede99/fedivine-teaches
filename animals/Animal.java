package animals;

public abstract class Animal implements Comparable<Animal> {
	protected int legs = 0;
	protected String name;

	public Animal() {
		this.name = "unnamed";
	}

	public Animal(String name) {
		this.name = name;
	}

	public int getLegs() {
		return this.legs;
	}

	public String getName() {
		return this.name;
	}

	public abstract void speak();

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " " + this.name + " has " + this.legs + " legs";
	}

	@Override
	public int compareTo(Animal other) {
		// return -1: this < other
		// return 0: this == other
		// return 1: this > other
		return this.name.compareTo(other.name);
	}
}
