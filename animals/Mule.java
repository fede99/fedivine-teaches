package animals;

import java.util.HashMap;
import java.util.Map;

import items.Kekse;

public class Mule extends Animal implements IPackingAnimal<Kekse> {

	private Map<String, Kekse> kekse = new HashMap<>();

	public Mule() {
		this("unnamed");
	}

	public Mule(String name) {
		super(name);
		this.legs = 4;
	}

	@Override
	public void speak() {
		System.out.println("Iiih Aaaah!!!");
	}

	@Override
	public void store(String key, Kekse item) {
		kekse.put(key, item);
	}

	@Override
	public Kekse retreive(String key) {
		if (!kekse.containsKey(key)) {
			throw new Error("Item not found.");
		}

		return kekse.get(key);
	}
}