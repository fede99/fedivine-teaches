import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import animals.*;

public class Program {
	public static void main(String[] args) {
//		try {
//			Animal[] animals = FileReader.parseAnimalsFromFile("files/animals.txt");
//			Arrays.sort(animals);
//		} catch (Exception e) {
//			System.out.println("An error occurred: " + e.getMessage());
//		}
		
		Set<String> names = new HashSet<String>() {
			{
				add("Liva");
				add("Gilles");
				add("Fede");
				add("Andrea");
			}
		};
		
		names
			.stream()
			.filter(name -> name.toLowerCase().contains("a"))
			.map(name -> new Dog(name))
			.forEach(name -> System.out.println(name));
		
		System.out.println(names);
			
	}
}
