import java.util.Collection;

import animals.Animal;

public final class AnimalInfo {
	public static void printInfo(Collection<Animal> animals) {
		// Print info for all animals
		for (Animal animal : animals) {
			printInfo(animal);
		}
	}

	public static void printInfo(Animal animal) {
		System.out.println("=============");
		System.out.println(animal.toString());
		animal.speak();

		// if (animal instanceof Dog) {
		// Dog dog = (Dog) animal;
		// dog.shit();
		// }

		// if (animal instanceof IPettable) {
		// IPettable pet = (IPettable) animal;
		// pet.pet();
		// pet.scold();
		// }

		// if (animal instanceof IFeedable) {
		// IFeedable feedable = (IFeedable) animal;
		// feedable.feed();
		// }
	}
}
