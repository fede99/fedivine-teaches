import animals.Animal;

interface IMyList<T> {
	public void add(T item);

	public T getLast();
}

interface IMyListNode<T> {
	T value();

	IMyListNode<T> next();
}

class MyList<T> implements IMyList<T> {

	@Override
	public void add(T item) {
		// TODO Auto-generated method stub

	}

	@Override
	public T getLast() {
		// TODO Auto-generated method stub
		return null;
	}

}

interface IMyNumberList<T extends Number> extends IMyList<T> {
	public T max();

	public T sum();
}

class MyNumberList<T extends Number> implements IMyNumberList<T> {

	@Override
	public void add(T item) {
		// TODO Auto-generated method stub

	}

	@Override
	public T getLast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T max() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T sum() {
		// TODO Auto-generated method stub
		return null;
	}

}